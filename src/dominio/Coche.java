/*![Copyrith](Copyrith.png)
>Copyright [2020] (Carlos Sainz, Sebastian Gines copyright)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package dominio;

public class Coche{
	private String nombre;
	private String marca;

	public Coche(){
		nombre = "";
		marca = "";
	}

	public Coche(String nombre, String mc){
		this.nombre = nombre;
		this.marca = mc;
	}

	public String getNombre(){
		return nombre;
	}

	public void setNombre(String nombre){
		this.nombre = nombre;
	}

	public String getMarca(){
		return marca;
	}

	public void setMarca(String mc){
		this.marca = mc;
	}

	public String toString(){
		return nombre + " " + marca + "\n";
	}
}

