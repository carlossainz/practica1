Public class Vehiculo{

        private Srtring tipoVehiculo;

        public Vehiculo(String tipoVehiculo){
                this.tipoVehiculo=tipoVehiculo;
        }

        public String getTipoVehiculo(){
                return tipoVehiculo;
        }

        public void setTipo(String tipoVehiculo){
                this.Vehiculo=tipoVehiculo;
        }

        /**Sobrecarga de metodo ToString POLIMORFISMO
         *
         * @return String con los datos de un vehiculo
         */

        @Override
                public String toString(){
                return tipoVehiculo;
                }

}

