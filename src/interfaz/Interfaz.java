/*![Copyrith](Copyrith.png)
>Copyright [2020] (Carlos Sainz, Sebastian Gines copyright)
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package interfaz;

import dominio.*;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileWriter;

public class Interfaz{
	private static String HELP_TEXT = "Texto de ayuda";
	private static String NOMBRE_FICHERO = "catalogoDeCoches.txt";

	public static void procesarPeticion(String sentencia){
		String[] args = sentencia.split(" ");
		Catalogo catalogo = inicializarCatalogo(NOMBRE_FICHERO);
		if(args[0].equals("help")){
			System.out.println(HELP_TEXT);
		} else if (args[0].equals("list")){
			if(catalogo.toString().equals("")){
				System.out.println("No se encuentra ningun coche en el catalogo");
			} else {
				System.out.println(catalogo);
			}
		} else if (args[0].equals("add")){
			Coche coche= new Coche(args[1], args[2]);
			catalogo.annadirCoche(coche);
			inicializarFichero(catalogo);
		}
	}

	private static void inicializarFichero(Catalogo catalogo){
		try{
			FileWriter fw = new FileWriter(NOMBRE_FICHERO);
			fw.write(catalogo.toString());
			fw.close();
		} catch (Exception e){
			System.out.println(e);
		}
	}

	private static Catalogo inicializarCatalogo(String nombreFichero){
		Catalogo catalogo = new Catalogo();
		try{
			File file = new File(nombreFichero);
			Scanner sc = new Scanner(file);
			while(sc.hasNext()){
				String nombre = sc.next();
				String mc = sc.next();
				Coche coche = new Coche(nombre, mc);
				catalogo.annadirCoche(coche);
			}
			sc.close();
		} catch (FileNotFoundException e){
			inicializarFichero(catalogo);
		} catch (Exception e){
			System.out.println(e);
		}
		return catalogo;
	}
}
